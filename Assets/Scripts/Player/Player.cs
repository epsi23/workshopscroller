using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private float speedMult;

    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    
    [SerializeField] private float m_JumpForce = 400f;							// Amount of force added when the player jumps.
    
    private List<IStatusEffect> statusEffects;
	
    [SerializeField] private Transform m_GroundCheck;							// A position marking where to check if the player is grounded.

    [SerializeField] private LayerMask m_WhatIsGround;							// A mask determining what is ground to the character
    
    [SerializeField] private Collider2D m_CrouchDisableCollider;				// A collider that will be disabled when crouching
    
    private bool m_Grounded;            // Whether or not the player is grounded.
    
    private Rigidbody2D m_Rigidbody2D;
    // Start is called before the first frame update
    
    private bool m_wasCrouching = false;
    private Animator m_Animator = null;

    void Start()
    {
	    //Get the Animator attached to the GameObject you are intending to animate.
	    m_Animator = gameObject.GetComponent<Animator>();
        
    }
    

    // Update is called once per frame
    void Update()
    {
	    m_Animator.SetBool("Jump", !m_Grounded);

	    if (Input.GetKeyDown(KeyCode.UpArrow) )
	    {
		    // print("Jump");
		    Move(false,true);

	    }	    
	    if (Input.GetKey(KeyCode.DownArrow) )
	    {
		    // print("Crouch");
		    Move(true,false);
		    m_Animator.SetBool("Crouch", true);
	    }
	    else
	    {
		    Move(false,false);
		    m_Animator.SetBool("Crouch", false);
	    }

	    // foreach (var effect in statusEffects)
	    // {
		   //  effect.OnUpdate(this);
	    // }
    }

    void AddEffect(IStatusEffect statusEffect)
    {
        statusEffect.ApplyEffect(this);
        statusEffects.Add(statusEffect);
    }

    void RemoveEffect(IStatusEffect statusEffect)
    {
        statusEffect.OnEndEffect(this);
        statusEffects.Remove(statusEffect);

    }
    private void Awake()
    {
	    m_Rigidbody2D = GetComponent<Rigidbody2D>();


    }
    private void FixedUpdate()
	{
		bool wasGrounded = m_Grounded;
		m_Grounded = false;

		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		// This can be done using layers instead but Sample Assets will not overwrite your project settings.
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				m_Grounded = true;
				// if (!wasGrounded)
				// 	OnLandEvent.Invoke();
			}
		}
		

	}


	public void Move(bool crouch, bool jump)
	{
		// // If the player should jump...

		if (m_Grounded)
		{
			if (jump)
			{
				// Add a vertical force to the player.
				m_Grounded = false;
				m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
			}

		
			// If crouching
			if (crouch)
			{
				if (!m_wasCrouching)
				{
					m_wasCrouching = true;
					// OnCrouchEvent.Invoke(true);
				}



				// Disable one of the colliders when crouching
				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = false;
			} else
			{	
				// Enable the collider when not crouching
				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = true;

				if (m_wasCrouching)
				{
					m_wasCrouching = false;
					// OnCrouchEvent.Invoke(false);
				}
			}
		}


	}
}
