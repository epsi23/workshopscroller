using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class Item : MonoBehaviour, ISpawnable
{
    [SerializeField]private float speed;
    [SerializeField]private SpawnManager.SpawnableItem type;
    [SerializeField] private bool canSpawnHigh;
    [SerializeField] private bool canSpawnMiddle;
    [SerializeField] private bool canSpawnLow;

    public StatusEffect[] statusEffects;

    public SpawnManager.SpawnableItem Type => type;
    
    public bool CanSpawnHigh => canSpawnHigh;
    public bool CanSpawnMiddle => canSpawnMiddle;
    public bool CanSpawnLow => canSpawnLow;


    public GameObject GetGameObject => this.gameObject;

    public float Speed
    {
        get => speed;
        set => speed = value;
    }
    public void OnUpdate()
    {
        transform.Translate(Vector3.left * Time.deltaTime * speed * GameStateManager.Instance.GameSpeed);
    }

    public void Create()
    {
        SpawnManager.Instance.Add(this);
    }

    public void Start()
    {
        Create();
    }
    // when the GameObjects collider arrange for this GameObject to travel to the left of the screen
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer == 6)
        {
            SpawnManager.Instance.DeleteSpawn(this);
        }
    }
}
