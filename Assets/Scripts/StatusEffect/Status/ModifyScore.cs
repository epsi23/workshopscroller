using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyScoreStatus : StatusEffect
{
    // Start is called before the first frame update
    [SerializeField] private int point;
    
    public void ApplyEffect(Player player)
    {
        GameStateManager.Instance.score += point;
    } 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
