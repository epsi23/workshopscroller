using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class StatusEffect : MonoBehaviour, IStatusEffect
{
    private bool over;
    private int duration;

    public int Duration => duration;
    public bool Over => over;
    public void ApplyEffect(Player player)
    {
        throw new System.NotImplementedException();
    }

    public void OnUpdate(Player player)
    {
        throw new System.NotImplementedException();
    }

    public void OnEndEffect(Player player)
    {
        throw new System.NotImplementedException();
    }
}
