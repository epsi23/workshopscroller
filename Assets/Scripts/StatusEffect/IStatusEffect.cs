using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStatusEffect
{
    public int Duration { get; }

    public bool Over { get; }

    public void ApplyEffect(Player player);
    public void OnUpdate(Player player);
    public void OnEndEffect(Player player);

}
