using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    static T instance = default(T);

    public static T Instance => instance;

    private void Awake()
    {
        Init();   
    }

    private void Init()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
            return;
        }
        instance = this as T;
        name += $"[{GetType().Name}]";
    }
}
