using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameStateManager : Singleton<GameStateManager>
{
    private const short maxSaves = 3;
    [SerializeField] private int gameSpeed = 1;

    private TextMeshProUGUI scoreText = null;
    [SerializeField] private GameObject scoreObj = null;

    [SerializeField] private GameObject saveObject = null;
    [SerializeField] private List<GameObject> saveObjs = new List<GameObject>();

    [SerializeField] private GameObject virusPercent = null;
    private TextMeshProUGUI protectText = null;

    private int protectPercent = 10;

    private int ProtectPercent
    {
        get => protectPercent;
        set
        {
            if ( value > 90)
            {
                protectPercent = 90;
            }
            else if ( value < 0)
            {
                protectPercent = 0;
            }
            else protectPercent = value;
            protectText.text = protectPercent + " %";
        } 
    }

    public int GameSpeed
    {
        get => gameSpeed;
        set => gameSpeed = value;
    }

    public int score = 0;
    public int Score
    {
        get => score;
        private set
        { 
            score = value;
            scoreText.text = value.ToString();
        }
    }

    public void AddProtectPercent(int addPercent)
    {
        if (addPercent < 0) return;
        ProtectPercent = protectPercent + addPercent ;
    }
    
    public void RemoveProtectPercent(int rmPercent)
    {
        if (rmPercent < 0) return;
        ProtectPercent = protectPercent - rmPercent ;
    }
    
    public void AddScore(int _score)
    {
        if (_score < 0) return;
        Score = score + _score;
    }
    
    public void RemoveScore(int _score)
    {
        if (_score < 0) return;
        Score = score - _score;
    }

    private List<int> saves = new List<int>();

    public void Save()
    {
        if (saves.Count >= maxSaves)
        {
            saves.RemoveAt(0);
        }
        else
        {
            saveObjs[saves.Count].gameObject.SetActive(true);
        }
        saves.Add(Score);
    }
    
    public void PushLastSave()
    {
        if (!saves.Any()) return;
        Score = saves[saves.Count - 1];
        CorruptLastSave();
    }

    public void CorruptLastSave()
    {
        if (!saves.Any()) return;
        int index = saves.Count - 1;
        saves.RemoveAt(index);
        saveObjs[index].gameObject.SetActive(false);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        if (scoreObj == null) Debug.LogError("Missing score object on GameStateManager");
        scoreText = scoreObj.GetComponent<TextMeshProUGUI>();
        protectText = virusPercent.GetComponent<TextMeshProUGUI>();
        Score = 0;
        for (int i = 0; i < maxSaves; i++)
        {
            GameObject save = saveObject.transform.Find($"Save{i+1}").gameObject;
            GameObject disabledObj = save.transform.Find("Disabled").gameObject;
            Image image = disabledObj.GetComponent<Image>();
            Color imageColor = image.color;
            imageColor.a = 0.5f;
            image.color = imageColor;
            GameObject enabledSave = save.transform.Find("Enabled").gameObject;
            enabledSave.SetActive(false);
            saveObjs.Add(enabledSave);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
