using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class BackgroundManager : Singleton<BackgroundManager>
{
    [SerializeField] private float SpeedMain = 400f;
    [SerializeField] private float SpeedSecondary = 400f;
    [SerializeField] private float groundSpeed = 600f;
    [SerializeField] private GameObject main = null;
    [SerializeField] private GameObject secondary = null;
    [SerializeField] private GameObject ground = null;
    [SerializeField] private float timeToReachTarget;
    
    [SerializeField] Vector3 startingPos = Vector3.zero;

    // // Update is called once per frame
    // void Update()
    // {
    //     main.transform.Translate(Vector3.left * Time.deltaTime * SpeedMain);
    //     secondary.transform.Translate(Vector3.left * Time.deltaTime * SpeedSecondary);
    //
    // }
    
    // public GameObject[] levels;
    [SerializeField] private Camera mainCamera;
    private Vector2 screenBounds;
    public float choke;
    private float t;
    private Vector3 startPosition;
    private Vector3 target;

    void Awake()
    {
        transform.position = startingPos;
    }
    void Start()
    {

        startPosition = target = main.transform.position; 
        SpriteRenderer spr = main.GetComponent<SpriteRenderer>();

        

        screenBounds = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, mainCamera.transform.position.z));
        target.x -= spr.bounds.size.x-(screenBounds.x*2);

        // foreach(GameObject obj in levels){
        loadChildObjects(secondary);
        loadChildObjects(ground);
        // }
    }
    void loadChildObjects(GameObject obj){
        float objectWidth = obj.GetComponent<SpriteRenderer>().bounds.size.x - choke;
        int childsNeeded = (int)Mathf.Ceil(screenBounds.x * 2 / objectWidth);
        GameObject clone = Instantiate(obj) as GameObject;
        for(int i = 0; i <= childsNeeded; i++){
            GameObject c = Instantiate(clone) as GameObject;
            c.transform.SetParent(obj.transform);
            c.transform.position = new Vector3(objectWidth * i, obj.transform.position.y, obj.transform.position.z);
            c.name = obj.name + i;
        }
        Destroy(clone);
        Destroy(obj.GetComponent<SpriteRenderer>());
    }
    void repositionChildObjects(GameObject obj){
        Transform[] children = obj.GetComponentsInChildren<Transform>();
        if(children.Length > 1){
            GameObject firstChild = children[1].gameObject;
            GameObject lastChild = children[children.Length - 1].gameObject;
            float halfObjectWidth = lastChild.GetComponent<SpriteRenderer>().bounds.extents.x - choke;
            if(mainCamera.transform.position.x + screenBounds.x > lastChild.transform.position.x + halfObjectWidth){
                firstChild.transform.SetAsLastSibling();
                firstChild.transform.position = new Vector3(lastChild.transform.position.x + halfObjectWidth * 2, lastChild.transform.position.y, lastChild.transform.position.z);
            }else if(mainCamera.transform.position.x - screenBounds.x < firstChild.transform.position.x - halfObjectWidth){
                lastChild.transform.SetAsFirstSibling();
                lastChild.transform.position = new Vector3(firstChild.transform.position.x - halfObjectWidth * 2, firstChild.transform.position.y, firstChild.transform.position.z);
            }
        }
    }
    void Update() {

        //Vector3 velocity = Vector3.zero;
        //Vector3 desiredPosition = secondary.transform.position - new Vector3(SpeedSecondary, 0, 0);
        //Vector3 smoothPosition = Vector3.SmoothDamp(secondary.transform.position, desiredPosition, ref velocity, 0.3f);
        //secondary.transform.position = smoothPosition;
        
        t += Time.deltaTime/timeToReachTarget;
        main.transform.position = Vector3.Lerp(startPosition, target, t);
        
        ground.transform.Translate(Vector3.left * Time.deltaTime * groundSpeed);

        secondary.transform.Translate(Vector3.left * Time.deltaTime * SpeedSecondary);
    }
    void LateUpdate(){
        repositionChildObjects(secondary);
        repositionChildObjects(ground);
    }
}
