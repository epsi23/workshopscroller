using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpawnable
{
    public SpawnManager.SpawnableItem Type { get; }

    public bool CanSpawnHigh { get; }
    public bool CanSpawnMiddle { get; }
    public bool CanSpawnLow { get; }
    public GameObject GetGameObject { get; }
    public float Speed { get; set; }

    public void OnUpdate();

    public void Create();
    
    
}
