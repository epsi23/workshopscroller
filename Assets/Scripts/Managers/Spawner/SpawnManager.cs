using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class SpawnManager : Singleton<SpawnManager>
{
    public enum SpawnableItem
    {
        Bad,
        Good
    }
    public enum SpawnPlace
    {
        
    }

    [SerializeField] private GameObject highSpawn = null;
    [SerializeField] private GameObject middleSpawn = null;
    [SerializeField] private GameObject lowSpawn = null;
    
    private List<ISpawnable> spawns = new List<ISpawnable>();
    private List<GameObject> spawnPoints = new List<GameObject>();

    private List<ISpawnable> list_goods = new List<ISpawnable>();
    private List<ISpawnable> list_obstacles = new List<ISpawnable>();
    private List<ISpawnable> list_bads = new List<ISpawnable>();

    private float waitTime = 2.0f;
    private float timer = 0.0f;
    
    public void Spawn( SpawnableItem type )
    {
        ISpawnable selected;
        GameObject spawnPoint;
        GameObject createdTmp;
        switch (type)
        {
            case SpawnableItem.Good:
                selected = list_goods [Random.Range(0, list_goods.Count)];
                spawnPoint = spawnPoints [Random.Range(0, spawnPoints.Count)];
                createdTmp = Instantiate(selected.GetGameObject);
                createdTmp.transform.position = new Vector3(x:spawnPoint.transform.position.x,y:spawnPoint.transform.position.y+(createdTmp.GetComponent<SpriteRenderer>().bounds.size.y/2),z:spawnPoint.transform.position.z);
                createdTmp.SetActive(true);
                break;            
            case SpawnableItem.Bad:
                selected = list_bads [Random.Range(0, list_bads.Count)];
                spawnPoint = spawnPoints [Random.Range(0, spawnPoints.Count)];
                createdTmp= Instantiate(selected.GetGameObject);
                createdTmp.transform.position = new Vector3(x:spawnPoint.transform.position.x,y:spawnPoint.transform.position.y+(createdTmp.GetComponent<SpriteRenderer>().bounds.size.y/2),z:spawnPoint.transform.position.z);
                createdTmp.SetActive(true);
                break;
        }
    }

    public void GenerateItems(Object[] objects,int type)
    {
        foreach (var good in objects)
        {
            GameObject go = GameObject.Instantiate(good) as GameObject;
            go.gameObject.SetActive(false);
            ISpawnable isp = go.GetComponent<Item>();
            if (type == 1)
            {
                list_goods.Add(isp);
            }
            if (type == 2)
            {
                list_obstacles.Add(isp);
            }
            if (type == 3)
            {
                list_bads.Add(isp);
            }
            
        }
    }

    public void DeleteSpawn(ISpawnable spawn)
    {
        if (!spawns.Contains(spawn)) return;
        spawns.Remove(spawn);
        Destroy(spawn.GetGameObject);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        spawnPoints.Add(highSpawn);
        spawnPoints.Add(middleSpawn);
        spawnPoints.Add(lowSpawn);
        GenerateItems(Resources.LoadAll("Items/Good"),1);
        GenerateItems(Resources.LoadAll("Items/Obstacles"),2);
        GenerateItems(Resources.LoadAll("Items/Bad"),3);
        if (highSpawn == null || middleSpawn == null || lowSpawn == null)
        {
            Debug.LogError("Missing spawn GameObject for SpawnManager");
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        // Check if we have reached beyond 2 seconds.
        // Subtracting two is more accurate over time than resetting to zero.
        if (timer > waitTime)
        {
            // Remove the recorded 2 seconds.
            timer = 0f;
            Spawn((SpawnableItem)Random.Range(0, Enum.GetValues(typeof(SpawnableItem)).Length));
        }

        
        foreach (var spawn in spawns)
        {
            spawn.OnUpdate();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(highSpawn.transform.position, 0.25f);
        Gizmos.DrawSphere(middleSpawn.transform.position, 0.25f);
        Gizmos.DrawSphere(lowSpawn.transform.position, 0.25f);
    }

    public void Add(ISpawnable spawnable)
    {
        spawns.Add(spawnable);
    }
    

}
