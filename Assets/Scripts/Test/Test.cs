using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField] private float waitTime = 10;
    [SerializeField] private bool testScore = false;
    [SerializeField] private bool testSaves = false;
    [SerializeField] private bool testPercent = false;

    private int timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += 1;
        if ( timer % waitTime == 0 && testScore )
        {
            //timer = 0;
            GameStateManager.Instance.AddScore(1);

        } 
        if ( timer % 100 == 0 && testSaves )
        {
            GameStateManager.Instance.Save();
        }
        else if (timer % 150 == 0 && testSaves)
        {
            GameStateManager.Instance.PushLastSave();
        }
        if (timer % 500 == 0 && testSaves)
        {
            GameStateManager.Instance.CorruptLastSave();
        }

        if (testPercent)
        {
            if (timer % 500 == 0)
            {
                GameStateManager.Instance.AddProtectPercent(10);
            }

            if (timer % 2500 == 0)
            {
                GameStateManager.Instance.RemoveProtectPercent(40);
            }
        }
        
    }
}
